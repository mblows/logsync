# SFTP Log Synchronisation Tool

This program is a bit of a hack to get the Suricata logs (or any logs, technically) from OPNSense to an Elastic server. It is intended to solve the problem of Filebeat failing to compile on FreeBSD/OPNSense, and the built-in Elastic integration being limited to reading a local `/var/log/suricata/eve.json`.

The program watches a specified file for changes, and when one occurs, it writes the changes via SSH and SFTP to a remote file.

Although it is intended to be relatively secure, I would not suggest using it on a production system. It is only intended as a workaround to allow visualising Suricata alerts in the cybersecurity industry project.

## Download Option 1: Pre-Built Binaries

To install the pre-build releases of this program, go to https://gitlab.com/mblows/logsync/-/releases (the "releases" link on the right) and copy the link for the latest release.

On your OPNSense machine, first install `wget`, then download the release:

```shell
pkg install wget
wget https://gitlab.com/mblows/logsync/uploads/39655e851c05ac33e13e7b45941144c4/logsync-0.1.0-freebsd-amd64.tar.gz
```

Then create a directory for it in your home directory and extract the files:

```shell
mkdir ~/logsync && chmod 700 ~/logsync
tar -xzvf logsync-0.1.0-freebsd-amd64.tar.gz -C ~/logsync
```

**Note:** Replace `logsync-0.1.0-freebsd-amd64.tar.gz` with the actual file name of the release.

You will now have the `logsync` executable, the `config.toml` configuration file and the `install.sh` installation script in `~/logsync`.

## Download Option 2: Compiling From Source Code

To compile this program, first enable the FreeBSD repositories in OPNSense:

```shell
sed -i '' 's/enabled: no/enabled: yes/' /usr/local/etc/pkg/repos/FreeBSD.conf
```

Then install git and the Go compiler, and disable the FreeBSD repositories again:

```shell
pkg install git go
sed -i '' 's/enabled: yes/enabled: no/' /usr/local/etc/pkg/repos/FreeBSD.conf
```

Next, clone the logsync GitLab repository and compile the program:

```shell
git clone https://gitlab.com/mblows/logsync.git ~/logsync
chmod 700 ~/logsync && cd ~/logsync
go build .
```

As with the above option, you will now have the `logsync` executable you just built, the `config.toml` configuration file and the `install.sh` installation script in `~/logsync`. There will also be multiple Go source files, but these are not needed for the program to run.

## Pre-Installation

Before using this tool, there are several steps required. It uses SSH and SFTP as its mechanism to transfer logs, and requires SSH keys to be set up to avoid the storage of passwords. To set up SSH keys, complete the following steps:

1. Log into your Ubuntu Server/Kali Purple machine, and set a new `root` password using `passwd`:
   
   ```
   sudo su
   passwd
   ```

2. Log into your OPNSense machine as `root`, and choose `8` to drop into a shell. Create a new SSH key pair using `ssh-keygen`, accepting all defaults:
   
   ```
   root@OPNsense:~ # ssh-keygen
   Generating public/private ed25519 key pair.
   Enter file in which to save the key (/root/.ssh/id_ed25519):
   Created directory '/root/.ssh'.
   Enter passphrase (empty for no passphrase):
   Enter same passphrase again:
   Your identification has been saved in /root/.ssh/id_ed25519
   Your public key has been saved in /root/.ssh/id_ed25519.pub
   The key fingerprint is:
   SHA256:YpP1lEPJoqwYfLCTKF9AzQCMqbQ7LHSLoQcZe7DAo80 root@OPNsense.localdomain
   The key's randomart image is:
   +--[ED25519 256]--+
   |=+o+     ...     |
   |*=o o   ..o.     |
   |=@o= . ...+      |
   |X+E.o oo o .     |
   |=+=*..= S .      |
   |o=+... o         |
   |...              |
   |                 |
   |                 |
   +----[SHA256]-----+
   ```
   
    Make a note of the location of the private key, stored here at `/root/.ssh/id_ed25519`.

3. Use `cat` to print the output of the generated public key, and copy the output (`ssh-ed25519` onwards)
   
   ```
   root@OPNsense:~ # cat ~/.ssh/id_ed25519.pub
   ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBvW2W9Zcmfx7vrcxwKK1zB+CvBm8rLZDgXDKdhR3fXm root@OPNsense.localdomain
   ```

4. From OPNSense, SSH into your Ubuntu Server/Kali Purple machine as `root`, and accept the SSH fingerprint:
   
   ```
   root@OPNsense:~ # ssh 192.168.100.200
   The authenticity of host '192.168.100.200 (192.168.100.200)' can't be established.
   ED25519 key fingerprint is SHA256:oj0sGXNvYtcgObUwpm4LjZe5GryRB0fj84SEF+3M3Ik.
   No matching host key fingerprint found in DNS.
   This key is not known by any other names.
   Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
   Warning: Permanently added '192.168.100.200' (ED25519) to the list of known hosts.
   ```

5. Edit the `~/.ssh/authorized_keys` file of the Ubuntu/Kali Purple machine using `nano` or another editor and add your OPNSense public key to the bottom, or use `echo` to append it:
   
   ```
   echo 'ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIBvW2W9Zcmfx7vrcxwKK1zB+CvBm8rLZDgXDKdhR3fXm root@OPNsense.localdomain' >> ~/.ssh/authorized_keys
   ```
   
    **Your key will be different**, do not blindly copy and paste the above.

6. Exit the Ubuntu Server/Kali Purple machine by typing `exit`, then try to SSH in to the same machine again. This time you should **not be prompted for a password**. If you are, you have not set up the keys correctly.

7. On your OPNSense machine you should now have the following:
   
   - A private key at `~/.ssh/id_ed25519`
   
   - A `known_hosts` file at `~/.ssh/known_hosts` containing the accepted fingerprints of remote machines. This is to [prevent MITM attacks](https://www.ssh.com/academy/attack/man-in-the-middle).

## Installation and Configuration

The program has a [TOML](https://toml.io/en/) configuration file:

```toml
localFile = "/var/log/suricata/eve.json"
sftpUser = "root"
privateKeyFile = "~/.ssh/id_ed25519"
knownHostsFile = "~/.ssh/known_hosts"
sftpHost = "192.168.100.200"
sftpPort = 22
remoteDir = "/var/log/suricata"
debounceDelay = 500
remoteUID = 0
remoteGID = 0
remoteFilePerms = "0600"
remoteDirPerms = "0700"
logFile = "~/logsync.log"
```

Most settings have sensible defaults and there is no need to change them. In particular, it is not recommended to change the `remoteFilePerms` or `remoteDirPerms` settings, as they are set to secure values. The `remoteUID` and `remoteGID` settings are also set to `0` (root) by default, but you may wish to change them to a different user if you are not running Elastic as root.

The settings to pay attention to in your setup are:

| Config Option | Description                                                                      |
| ------------- | -------------------------------------------------------------------------------- |
| `localFile`   | The location of the local file to monitor for changes                            |
| `sftpHost`    | The IP address of your Ubuntu Server/Kali Purple machine, with Elastic installed |
| `remoteDir`   | The remote folder to copy the `localFile` into.                                  |

The other settings are as follows:

| Config Option     | Description                                                                                                                                    |
| ----------------- | ---------------------------------------------------------------------------------------------------------------------------------------------- |
| `sftpUser`        | The username for authenticating with the SFTP server.                                                                                          |
| `privateKeyFile`  | The path to the private key file used for SSH authentication.                                                                                  |
| `knownHostsFile`  | The path to the known hosts file for SSH host key verification.                                                                                |
| `sftpPort`        | The port number on which the SFTP server is listening (default: 22).                                                                           |
| `debounceDelay`   | The delay (in milliseconds) between file modifications to prevent excessive transfers.                                                         |
| `remoteUID`       | The user ID (UID) to set for the transferred file on the remote system.                                                                        |
| `remoteGID`       | The group ID (GID) to set for the transferred file on the remote system.                                                                       |
| `remoteFilePerms` | The file permissions to set for the transferred file on the remote system (in octal notation).<br/>Executable permission bits are not allowed. |
| `remoteDirPerms`  | The directory permissions to set for the remote directory (in octal notation).                                                                 |
| `logFile`         | The path to the log file where the program's activity will be recorded.                                                                        |

In the OPNSense terminal, test that the program is working properly by running it from the OPNSense command line:

```
root@OPNsense:~/logsync # ./logsync
2024/04/07 14:08:19 Watching for changes to /var/log/suricata/eve.json
```

In the OPNSense web GUI, ensure that Suricata is enabled and running by going to **Services** ➜ **Intrusion Detection**, and download and enable the "abuse.ch/URLhaus" rules to test.

Meanwhile, on your Ubuntu Server/Kali Purple machine, open the terminal, and access a [malware domain](https://urlhaus.abuse.ch/browse/):

```shell
wget http://27.215.209.102:38558/bin.sh -O /dev/null
```

**Ensure you output the file to `/dev/null` because this is real malware**.

You should get feedback from logsync telling you that a change was detected and written to the remote server:

```
root@OPNsense:~/logsync # ./logsync
2024/04/07 18:22:42 Watching for changes to /var/log/suricata/eve.json
File modified: /var/log/suricata/eve.json
2024/04/07 18:22:47 Connecting to root@192.168.100.200:22
2024/04/07 18:22:47 File /var/log/suricata/eve.json transferred to 192.168.100.200:/var/log/suricata
2024/04/07 18:22:47 Permissions set to 0600, UID to 0, GID to 0
```

You can now access your Elastic instance and enable the Suricata integration, using the default path of `/var/log/suricata`.

## OPNSense Startup Script

If the above test works without errors, you can now install the startup script to enable logsync to start with the system. To do this, simply run the `install.sh` file in the same directory as the executable:

```shell
cd ~/logsync
chmod +x install.sh
./install.sh
```

Please be aware that if you move the logsync directory, you will have to run the script again.

If you need to see program output, it is written to `~/logsync.log` by default, and you can watch the output in real time by running `tail -f ~/logsync.log`.

You can start, stop, enable and disable the service by typing:

```shell
service logsync start   # Start the logsync service
service logsync stop    # Stop the logsync service
service logsync status  # Get the status of the service
service logsync enable  # Enable the service on startup
service logsync disable # Disable the service on startup
```
