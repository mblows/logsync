package main

import (
	"fmt"
	"net"
	"os"
	"path/filepath"
	"reflect"
	"regexp"
	"strings"

	"github.com/pelletier/go-toml/v2"
)

const (
	DefaultSFTPPort        = 22                   // Default SFTP port
	DefaultDebounceDelay   = 500                  // Default debounce delay in milliseconds
	DefaultPrivateKeyFile  = "~/.ssh/id_rsa"      // Default private key file
	DefaultKnownHostsFile  = "~/.ssh/known_hosts" // Default known hosts file
	DefaultLogFile         = "~/logsync.log"      // Default log file
	DefaultRemoteFilePerms = "0600"               // Default remote file permissions
	DefaultRemoteDirPerms  = "0700"               // Default remote directory permissions
	DefaultRemoteUID       = 0                    // Default remote user ID
	DefaultRemoteGID       = 0                    // Default remote group ID
)

type Config struct {
	LocalFile       string `toml:"localFile"`       // Path to the local file to sync
	SFTPUser        string `toml:"sftpUser"`        // Username for SFTP authentication
	SFTPHost        string `toml:"sftpHost"`        // SFTP server IP address
	SFTPPort        int    `toml:"sftpPort"`        // SFTP server port
	PrivateKeyFile  string `toml:"privateKeyFile"`  // Path to the private key file
	KnownHostsFile  string `toml:"knownHostsFile"`  // Path to the known hosts file
	RemoteDir       string `toml:"remoteDir"`       // Remote directory to sync the file to
	DebounceDelay   int    `toml:"debounceDelay"`   // Debounce delay in milliseconds
	RemoteUID       int    `toml:"remoteUID"`       // Remote user ID
	RemoteGID       int    `toml:"remoteGID"`       // Remote group ID
	RemoteFilePerms string `toml:"remoteFilePerms"` // Remote file permissions
	RemoteDirPerms  string `toml:"remoteDirPerms"`  // Remote directory permissions
	LogFile         string `toml:"logFile"`         // Path to this program's log file
}

// expandTilde expands a path that starts with a tilde (~) to the user's home directory.
func expandTilde(path string) (string, error) {
	path = strings.TrimSpace(path)
	if path[:1] == "~" {
		homeDir, err := os.UserHomeDir()
		if err != nil {
			return "", err
		}

		return filepath.Clean(filepath.Join(homeDir, path[1:])), nil
	}
	return path, nil
}

// validateIP checks if the given string is a valid IP address.
func validateIP(ip string) error {
	if net.ParseIP(ip) == nil {
		return fmt.Errorf("invalid IP address: %s", ip)
	}
	return nil
}

// LoadConfig loads the configuration from the specified file.
func LoadConfig(path string) (*Config, error) {
	data, err := os.ReadFile(path)
	if err != nil {
		return nil, err
	}

	var config Config
	if err = toml.Unmarshal(data, &config); err != nil {
		return nil, fmt.Errorf("failed to parse config: %w", err)
	}

	// Trim whitespace from all string fields
	v := reflect.ValueOf(&config).Elem()
	for i := 0; i < v.NumField(); i++ {
		if v.Field(i).Kind() == reflect.String {
			v.Field(i).SetString(strings.TrimSpace(v.Field(i).String()))
		}
	}

	// Validate required fields, make sure they are not empty
	if config.LocalFile == "" || config.SFTPHost == "" || config.RemoteDir == "" {
		return nil, fmt.Errorf("missing required config fields")
	}

	// Expand all paths that start with a tilde (~)
	for _, path := range []*string{&config.LocalFile, &config.PrivateKeyFile, &config.KnownHostsFile, &config.LogFile} {
		if expanded, err := expandTilde(*path); err != nil {
			return nil, err
		} else {
			*path = expanded
		}
	}

	// Make sure the watch directory exists
	localDir := filepath.Dir(config.LocalFile)
	if _, err := os.Stat(localDir); os.IsNotExist(err) {
		return nil, fmt.Errorf("watch directory %s does not exist", localDir)
	}

	// Make sure the log file can be opened for writing
	if _, err := os.OpenFile(config.LogFile, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0600); err != nil {
		return nil, fmt.Errorf("failed to open log file for writing: %s", config.LogFile)
	}

	// Verify that the private key file exists
	if _, err := os.Stat(config.PrivateKeyFile); os.IsNotExist(err) {
		return nil, fmt.Errorf("private key file %s does not exist", config.PrivateKeyFile)
	}

	// Verify that the known hosts file exists
	if _, err := os.Stat(config.KnownHostsFile); os.IsNotExist(err) {
		return nil, fmt.Errorf("known hosts file %s does not exist", config.KnownHostsFile)
	}

	// Set default values for optional fields
	if config.SFTPUser == "" {
		config.SFTPUser = os.Getenv("USER")
	}

	// Validate the SFTP IP address
	if err := validateIP(config.SFTPHost); err != nil {
		return nil, err
	}

	// Validate the SFTP port
	if config.SFTPPort <= 0 || config.SFTPPort > 65535 {
		config.SFTPPort = DefaultSFTPPort
	}

	// Validate the remote user ID
	if config.PrivateKeyFile == "" {
		config.PrivateKeyFile = DefaultPrivateKeyFile
	}

	// Validate the debounce delay
	if config.DebounceDelay <= 0 {
		config.DebounceDelay = DefaultDebounceDelay
	}

	// Validate the remote file permissions
	if config.RemoteFilePerms == "" {
		config.RemoteFilePerms = DefaultRemoteFilePerms
	}

	// Validate the remote directory permissions
	if config.RemoteDirPerms == "" {
		config.RemoteDirPerms = DefaultRemoteDirPerms
	}

	// Validate the known hosts file location
	if config.KnownHostsFile == "" {
		config.KnownHostsFile = DefaultKnownHostsFile
	}

	// Validate the log file location
	if config.LogFile == "" {
		config.LogFile = DefaultLogFile
	}

	// Validate the remote user ID
	if config.RemoteUID < 0 {
		config.RemoteUID = DefaultRemoteUID
	}

	// Validate the remote group ID
	if config.RemoteGID < 0 {
		config.RemoteGID = DefaultRemoteGID
	}

	// Validate the remote file permissions, ensuring it is a valid octal number
	// and doesn't have any execute bits set (for security reasons)
	if match, _ := regexp.MatchString("^0[0,2,4,6]{3}$", strings.TrimSpace(config.RemoteFilePerms)); !match {
		return nil, fmt.Errorf("invalid remote file permissions: %s", config.RemoteFilePerms)
	}

	// Validate the remote directory permissions, ensuring it is a valid octal number
	if match, _ := regexp.MatchString("^0[0-7]{3}$", strings.TrimSpace(config.RemoteDirPerms)); !match {
		return nil, fmt.Errorf("invalid remote directory permissions: %s", config.RemoteDirPerms)
	}

	return &config, nil
}
