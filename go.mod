module gitlab.com/mblows/logsync

go 1.20

require github.com/fsnotify/fsnotify v1.7.0

require (
	github.com/kr/fs v0.1.0 // indirect
	github.com/xo/terminfo v0.0.0-20210125001918-ca9a967f8778 // indirect
	golang.org/x/crypto v0.22.0 // indirect
)

require (
	github.com/gookit/color v1.5.4
	github.com/pelletier/go-toml/v2 v2.2.0
	github.com/pkg/sftp v1.13.6
	golang.org/x/sys v0.19.0 // indirect
)
