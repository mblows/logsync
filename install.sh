#!/bin/sh

# Get the current directory path
CURRENT_DIR=$(pwd)

SERVICE_SCRIPT="/usr/local/etc/rc.d/logsync"

# Create the service script
cat >"$SERVICE_SCRIPT" <<EOF
#!/bin/sh

# PROVIDE: logsync
# REQUIRE: NETWORKING
# KEYWORD: shutdown

. /etc/rc.subr

name="logsync"
rcvar="logsync_enable"
command="$CURRENT_DIR/logsync"
command_args="-c $CURRENT_DIR/config.toml"

export HOME="$HOME"
export USER="$USER"

load_rc_config \$name
run_rc_command "\$1"
EOF

# Make the service script executable
chmod +x "$SERVICE_SCRIPT"

# Enable the service script
if grep -q "^logsync_enable=" /etc/rc.conf; then
    sed -i '' 's/^logsync_enable=.*/logsync_enable="YES"/' /etc/rc.conf
else
    echo 'logsync_enable="YES"' >>/etc/rc.conf
fi

echo "Service script for logsync has been created and enabled."
