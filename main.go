package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"
	"time"

	"github.com/fsnotify/fsnotify"
)

// newLogger initializes a logger that writes to the specified log file, as well as to stdout.
func newLogger(logFile string) (*log.Logger, error) {
	file, err := os.OpenFile(logFile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		return nil, err
	}

	writer := io.MultiWriter(file, os.Stdout)
	logger := log.New(writer, "", log.Ldate|log.Ltime)

	return logger, nil
}

// watchFileChanges watches for changes to the local file and triggers a file transfer when a change is detected.
func watchFileChanges(watcher *fsnotify.Watcher, config *Config, stopChan <-chan bool, logger *log.Logger) {
	var debounceTimer *time.Timer
	for {
		select {
		case event := <-watcher.Events:
			// Only trigger on write events for the local file
			if event.Has(fsnotify.Write) && event.Name == config.LocalFile {
				if debounceTimer != nil {
					debounceTimer.Stop()
				}

				// If multiple write events are received within the debounce delay, only the last one will trigger the transfer
				debounceTimer = time.AfterFunc(time.Duration(config.DebounceDelay)*time.Millisecond, func() {
					fmt.Printf("File modified: %s\n", event.Name)
					if err := transferFile(config, logger); err != nil {
						logger.Printf("Error transferring file: %v\n", err)
					}
				})
			}
		case err := <-watcher.Errors:
			logger.Println(err)
		case <-stopChan:
			return
		}
	}
}

func main() {
	// Parse command-line flags
	configPath := flag.String("c", "config.toml", "Path to the configuration file")
	flag.Parse()

	// Load the configuration
	config, err := LoadConfig(*configPath)
	if err != nil {
		fmt.Printf("Error loading config: %v\n", err)
		os.Exit(1)
	}

	// Create a logger that writes to the log file and stdout
	logger, err := newLogger(config.LogFile)
	if err != nil {
		fmt.Printf("Error creating logger: %v\n", err)
		os.Exit(1)
	}

	// Watch for changes to the local file
	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		logger.Printf("Error creating watcher: %v\n", err)
		os.Exit(1)
	}
	defer watcher.Close()

	// Watch the directory containing the local file, as if a individual file is
	// deleted the watcher will be removed
	localDir := filepath.Dir(config.LocalFile)
	if err = watcher.Add(localDir); err != nil {
		logger.Printf("Error watching %s: %v\n", localDir, err)
		os.Exit(1)
	}

	logger.Printf("Watching for changes to %s\n", config.LocalFile)

	// Watch files for changes in a separate goroutine
	stopChan := make(chan bool)
	go watchFileChanges(watcher, config, stopChan, logger)

	// Listen for SIGINT or SIGTERM to stop the program gracefully
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGINT, syscall.SIGTERM)

	<-sigChan
	close(stopChan)
	fmt.Println("\x1b[2K\rStopping and removing watcher...")
}
