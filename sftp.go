package main

import (
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"github.com/pkg/sftp"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/knownhosts"
)

// createSSHConnection creates an SSH connection to the SFTP server using the provided configuration.
func createSSHConnection(config *Config) (*ssh.Client, error) {
	// Read the private key file
	privateKey, err := os.ReadFile(config.PrivateKeyFile)
	if err != nil {
		return nil, fmt.Errorf("failed to read private key file %s", config.PrivateKeyFile)
	}

	// Parse the private key
	signer, err := ssh.ParsePrivateKey(privateKey)
	if err != nil {
		return nil, fmt.Errorf("failed to parse private key %s", config.PrivateKeyFile)
	}

	// Create a host key callback function
	hostKeyCallback, err := knownhosts.New(config.KnownHostsFile)
	if err != nil {
		return nil, err
	}

	sshConfig := &ssh.ClientConfig{
		User:            config.SFTPUser,
		Auth:            []ssh.AuthMethod{ssh.PublicKeys(signer)},
		HostKeyCallback: hostKeyCallback,
		Timeout:         3 * time.Second,
	}

	// Connect to the SSH server with retry logic
	var conn *ssh.Client
	conn, err = ssh.Dial("tcp", fmt.Sprintf("%s:%d", config.SFTPHost, config.SFTPPort), sshConfig)
	if err != nil {
		return nil, err
	}

	return conn, nil
}

// createRemoteDirectory creates the remote directory on the SFTP server if it does not already exist.
func createRemoteDirectory(client *sftp.Client, remoteDir string, perms os.FileMode) error {
	// Check if the remote directory exists
	_, err := client.Stat(remoteDir)
	if err != nil {
		if os.IsNotExist(err) {
			// Remote directory does not exist, create it with the specified permissions
			if err := client.MkdirAll(remoteDir); err != nil {
				return fmt.Errorf("failed to create remote directory. Ensure you have remote write permissions")
			}
			if err = client.Chmod(remoteDir, perms); err != nil {
				return fmt.Errorf("failed to set remote directory permissions")
			}
		} else {
			return fmt.Errorf("failed to check remote directory existence: %v", err)
		}
	}
	return nil
}

// transferFile transfers the local file to the remote SFTP server.
func transferFile(config *Config, logger *log.Logger) error {
	var conn *ssh.Client
	var err error

	// Retry creating the SSH connection up to 3 times
	for i := 0; i < 3; i++ {
		logger.Printf("Connecting to %s@%s:%d\n", config.SFTPUser, config.SFTPHost, config.SFTPPort)
		conn, err = createSSHConnection(config)
		if err == nil {
			break
		}
		logger.Printf("Failed to create SSH connection, retrying in 5 seconds.\n")
		time.Sleep(5 * time.Second)
	}

	if err != nil {
		return err
	}
	defer conn.Close()

	// Create an SFTP client
	client, err := sftp.NewClient(conn)
	if err != nil {
		return err
	}
	defer client.Close()

	// Check if the local file exists
	if _, err := os.Stat(config.LocalFile); os.IsNotExist(err) {
		return fmt.Errorf("local file %s does not exist", config.LocalFile)
	}

	// Open the local file
	file, err := os.Open(config.LocalFile)
	if err != nil {
		return err
	}
	defer file.Close()

	remoteFile := filepath.Join(config.RemoteDir, filepath.Base(config.LocalFile))

	// Create the remote directory if it doesn't exist
	dirPerms, err := strconv.ParseUint(config.RemoteDirPerms, 8, 32)
	if err != nil {
		return fmt.Errorf("failed to parse remote directory permissions")
	}
	if err := createRemoteDirectory(client, config.RemoteDir, os.FileMode(dirPerms)); err != nil {
		return err
	}

	// Create remote file
	dstFile, err := client.Create(remoteFile)
	if err != nil {
		return fmt.Errorf("failed to create remote file. Ensure remote directory exists and you have write permissions")
	}
	defer dstFile.Close()

	// Copy file contents
	if _, err := io.Copy(dstFile, file); err != nil {
		return err
	}

	logger.Printf("File %s transferred to %s:%s\n", config.LocalFile, config.SFTPHost, config.RemoteDir)

	// Parse the file permissions string as an octal number
	filePerms, err := strconv.ParseUint(config.RemoteFilePerms, 8, 32)
	if err != nil {
		return fmt.Errorf("failed to parse remote file permissions")
	}

	// Set the permissions of the remote file
	if err = dstFile.Chmod(os.FileMode(filePerms)); err != nil {
		return fmt.Errorf("failed to set remote file permissions")
	}

	// Set the user and group ownership of the remote file
	if err = dstFile.Chown(config.RemoteUID, config.RemoteGID); err != nil {
		return err
	}

	logger.Printf("Permissions set to %s, UID to %d, GID to %d\n", config.RemoteFilePerms, config.RemoteUID, config.RemoteGID)

	return nil
}
